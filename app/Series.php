<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    public $timestamps = false;

    public function seasons() {
        return $this->hasMany('App\Season');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'user_series')->using('App\UserSeries');
    }
}
