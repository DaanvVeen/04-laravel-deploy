<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Season;

class Episode extends Model
{
    public $timestamps = false;

    public function season() {
        return $this->belongsTo('App\Season');
    }

    public function users() {
        return $this->hasMany('App\User', 'user_episodes')->using('App\UserEpisode');
    }

    public function next() {
        $next = Episode::where('season_id', '=', $this->season_id)
            ->where('number', '=', $this->number + 1)
            ->first();
        
        if ($next == null) {
            // The season is ended
            $series = $this->season()->first()->series()->first();

            $nextSeason = Season::where('series_id', '=', $series->id)
                ->where('number', '=', $this->season()->first()->number + 1)
                ->first();

            $next = Episode::where('season_id', '=', $nextSeason->id)
                ->where('number', '=', 1)
                ->first();
            $next->season = $nextSeason;

            return $next;
        }

        $next->season = $this->season;
        return $next;
    }

}
