<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserEpisode extends Pivot
{
    protected $table = "user_episodes";
    public $timestamps = false;
}
