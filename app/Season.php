<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    public $timestamps = false;

    public function episodes() {
        return $this->hasMany('App\Episode');
    }

    public function series() {
        return $this->belongsTo('App\Series');
    }
}
