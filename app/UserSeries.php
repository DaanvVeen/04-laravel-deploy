<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserSeries extends Pivot
{
    protected $table = "user_series";
    public $timestamps = false;
}
