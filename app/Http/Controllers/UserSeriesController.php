<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\UserSeries;
use App\User;
use App\Series;
use App\Season;
use App\UserEpisode;
use App\Episode;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserSeriesController extends Controller
{
    public function getFollowing(Request $request) {
        $user = JWTAuth::toUser($request->bearerToken());

        $series = User::findOrFail($user->id)
            ->series()
            ->get(['id', 'title', 'poster', 'plot_' . App::getLocale() . ' as plot']);
        
        $res = [];

        foreach ($series as $elem) {
            $userSeries = UserSeries::where('user_id', '=', $user->id)
                ->where('series_id', '=', $elem->id)
                ->first();

            if ($userSeries->finished) continue;

            $seasons = Season::where('series_id', '=', $elem->id)
                ->orderBy('number', 'desc')
                ->get();
            
            foreach ($seasons as $season) {
                $episodes = Episode::select('id')
                    ->where('season_id', '=', $season->id)
                    ->get();

                $lastSeenEpisode = User::findOrFail($user->id)
                    ->seenEpisodes()
                    ->whereIn('episode_id', $episodes)
                    ->orderBy('number', 'desc')
                    ->first();

                if ($lastSeenEpisode != null) {
                    $nextEpisode = $lastSeenEpisode->next();
                    $nextEpisode->season = $nextEpisode->season()->first();

                    $elem->next_episode = $nextEpisode;
                    break;
                }
            }

            if ($elem->next_episode == null) {
                $season = $elem->seasons()->orderBy('number', 'asc')->first();

                $elem->next_episode = $season->episodes()->orderBy('number', 'asc')->first();
                $elem->next_episode->season = $season;
            }

            array_push($res, $elem);
            
        }
        
        return $res;
    }

    public function follow(Request $request, $id) {
        $user = JWTAuth::toUser($request->bearerToken());

        $model = new UserSeries();
        $model->user_id = $user->id;
        $model->series_id = $id;
        $model->saveOrFail();

        return $model;
    }

    public function unfollow(Request $request, $id) {
        $user = JWTAuth::toUser($request->bearerToken());

        $model = UserSeries::where('user_id', '=', $user->id)
            ->where('series_id', '=', $id)
            ->delete();

        if ($model == 1) {
            return response('', 200);
        } else {
            return response('', 404);
        }
    }

    public function finished(Request $request, $id) {
        $user = JWTAuth::toUser($request->bearerToken());

        $model = UserSeries::where('user_id', '=', $user->id)
            ->where('series_id', '=', $id)
            ->update(['finished' => true]);

        if ($model == 1) {
            return response('', 200);
        } else {
            return response('', 404);
        }
    }
}
