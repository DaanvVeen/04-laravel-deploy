<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Series;
use App;
use Tymon\JWTAuth\Facades\JWTAuth;

class SeriesController extends Controller
{
    public function all() {
        return Series::all();
    }

    public function search($keyword) {
        return Series::where('title', 'LIKE', '%' . $keyword . '%')
            ->get();
    }

    public function details(Request $request, $id) {
        $user = JWTAuth::toUser($request->bearerToken());

        $language = $request->get('language', 'en');
        if (!in_array($language, ['en', 'nl'])) {
            App::setLocale('en');
        } else {
            App::setLocale($language);
        }

        $series = Series::select('id', 'poster', 'title', 'plot_' . App::getLocale() . ' as plot')
            ->where('id', '=', $id)
            ->firstOrFail();
        
        $seasons = Series::find($id)
            ->seasons()
            ->get();

        foreach ($seasons as $season) {
            $season->episodes = $season->episodes()->get();
        }

        $series->seasons = $seasons;

        $following = Series::findOrFail($id)
            ->users()
            ->where('id', '=', $user->id)
            ->first();

        $series->following = $following != null;

        return $series;
    }

    public function seasons($id) {
        return Series::findOrFail($id)
            ->seasons()
            ->get();
    }
}
