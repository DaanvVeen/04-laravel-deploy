<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\UserEpisode;
use App\UserSeries;
use App\Episode;
use App\Series;
use Illuminate\Support\Facades\Validator;

class EpisodeController extends Controller
{
    public function seen(Request $request, $id) {
        $user = JWTAuth::toUser($request->bearerToken());

        if ($user == null) {
            return response()->json(['error' => 'user not found'], 401);
        }

        $series = Episode::findOrFail($id)
            ->season()->first()
            ->series()->first();

        $model = new UserEpisode();
        $model->user_id = $user->id;
        $model->episode_id = $id;
        $model->saveOrFail();

        $lastEp = Series::findOrFail($series->id)
            ->seasons()
            ->orderBy('number', 'desc')->first()
            ->episodes()
            ->orderBy('number', 'desc')->first();
        
        if ($lastEp->id == $id) {
            $userSeries = UserSeries::where('series_id', '=', $series->id)
                ->where('user_id', '=', $user->id)
                ->update(['finished' => true]);
                
            return response()->json([
                'finished' => true
            ]);
        }

        return Episode::findOrFail($id)->next();
    }
} 
