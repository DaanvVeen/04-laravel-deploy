<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Season;

class SeasonsController extends Controller
{
    public function episodes($id) {
        return Season::findOrFail($id)
            ->episodes()
            ->get();
    }
}
