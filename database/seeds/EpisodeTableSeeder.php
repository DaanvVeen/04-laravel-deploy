<?php

use Illuminate\Database\Seeder;

class EpisodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $episodes = [[
            "id"=> 1,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 3
        ], [
            "id"=> 2,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 3
        ], [
            "id"=> 3,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 3
        ], [
            "id"=> 4,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 3
        ], [
            "id"=> 5,
            "number"=> 5,
            "title"=> "Episode 5",
            "season_id"=> 3
        ], [
            "id"=> 6,
            "number"=> 6,
            "title"=> "Episode 6",
            "season_id"=> 3
        ], [
            "id"=> 7,
            "number"=> 1,
            "title"=> "Er is daar een mijnheer",
            "season_id"=> 2
        ], [
            "id"=> 8,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 2
        ], [
            "id"=> 9,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 2
        ], [
            "id"=> 10,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 2
        ], [
            "id"=> 11,
            "number"=> 5,
            "title"=> "Jos, maak me nu los",
            "season_id"=> 2
        ], [
            "id"=> 12,
            "number"=> 6,
            "title"=> "The Code from a Dream from Mama",
            "season_id"=> 2
        ], [
            "id"=> 13,
            "number"=> 1,
            "title"=> "Een Interessante Investering",
            "season_id"=> 1
        ], [
            "id"=> 14,
            "number"=> 2,
            "title"=> "Mijn ma was mee",
            "season_id"=> 1
        ], [
            "id"=> 15,
            "number"=> 3,
            "title"=> "Operatie Plukketuffers",
            "season_id"=> 1
        ], [
            "id"=> 16,
            "number"=> 4,
            "title"=> "What is that with the washing machine?",
            "season_id"=> 1
        ], [
            "id"=> 17,
            "number"=> 5,
            "title"=> "Tot 7x70 maal",
            "season_id"=> 1
        ], [
            "id"=> 18,
            "number"=> 6,
            "title"=> "En geen flikken!",
            "season_id"=> 1
        ], [
            "id"=> 81,
            "number"=> 1,
            "title"=> "We're Back",
            "season_id"=> 5
        ], [
            "id"=> 82,
            "number"=> 2,
            "title"=> "Aikido",
            "season_id"=> 5
        ], [
            "id"=> 83,
            "number"=> 3,
            "title"=> "48 Meters Underground",
            "season_id"=> 5
        ], [
            "id"=> 84,
            "number"=> 4,
            "title"=> "It's Dolphin Time",
            "season_id"=> 5
        ], [
            "id"=> 85,
            "number"=> 5,
            "title"=> "Boom, Boom, Ciao",
            "season_id"=> 5
        ], [
            "id"=> 86,
            "number"=> 6,
            "title"=> "Everything Seemed Insignificant",
            "season_id"=> 5
        ], [
            "id"=> 87,
            "number"=> 7,
            "title"=> "A Quick Vacation",
            "season_id"=> 5
        ], [
            "id"=> 88,
            "number"=> 8,
            "title"=> "Astray",
            "season_id"=> 5
        ], [
            "id"=> 89,
            "number"=> 9,
            "title"=> "Game Over",
            "season_id"=> 5
        ], [
            "id"=> 90,
            "number"=> 10,
            "title"=> "Berlin's Wedding",
            "season_id"=> 5
        ], [
            "id"=> 91,
            "number"=> 11,
            "title"=> "Anatomy Lesson",
            "season_id"=> 5
        ], [
            "id"=> 92,
            "number"=> 12,
            "title"=> "Pasodoble",
            "season_id"=> 5
        ], [
            "id"=> 93,
            "number"=> 13,
            "title"=> "5 Minutes Earlier",
            "season_id"=> 5
        ], [
            "id"=> 94,
            "number"=> 14,
            "title"=> "TKO",
            "season_id"=> 5
        ], [
            "id"=> 95,
            "number"=> 15,
            "title"=> "Strike the Tent",
            "season_id"=> 5
        ], [
            "id"=> 96,
            "number"=> 16,
            "title"=> "The Paris Plan",
            "season_id"=> 5
        ], [
            "id"=> 97,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 4
        ], [
            "id"=> 98,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 4
        ], [
            "id"=> 99,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 4
        ], [
            "id"=> 100,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 4
        ], [
            "id"=> 101,
            "number"=> 5,
            "title"=> "Episode 5",
            "season_id"=> 4
        ], [
            "id"=> 102,
            "number"=> 6,
            "title"=> "Episode 6",
            "season_id"=> 4
        ], [
            "id"=> 103,
            "number"=> 7,
            "title"=> "Episode 7",
            "season_id"=> 4
        ], [
            "id"=> 104,
            "number"=> 8,
            "title"=> "Episode 8",
            "season_id"=> 4
        ], [
            "id"=> 105,
            "number"=> 9,
            "title"=> "Episode 9",
            "season_id"=> 4
        ], [
            "id"=> 106,
            "number"=> 10,
            "title"=> "Episode 10",
            "season_id"=> 4
        ], [
            "id"=> 107,
            "number"=> 11,
            "title"=> "Episode 11",
            "season_id"=> 4
        ], [
            "id"=> 108,
            "number"=> 12,
            "title"=> "Episode 12",
            "season_id"=> 4
        ], [
            "id"=> 109,
            "number"=> 13,
            "title"=> "Episode 13",
            "season_id"=> 4
        ], [
            "id"=> 110,
            "number"=> 14,
            "title"=> "Episode 14",
            "season_id"=> 4
        ], [
            "id"=> 111,
            "number"=> 15,
            "title"=> "Episode 15",
            "season_id"=> 4
        ], [
            "id"=> 112,
            "number"=> 1,
            "title"=> "No Más",
            "season_id"=> 8
        ], [
            "id"=> 113,
            "number"=> 2,
            "title"=> "Caballo sin Nombre",
            "season_id"=> 8
        ], [
            "id"=> 114,
            "number"=> 3,
            "title"=> "I.F.T.",
            "season_id"=> 8
        ], [
            "id"=> 115,
            "number"=> 4,
            "title"=> "Green Light",
            "season_id"=> 8
        ], [
            "id"=> 116,
            "number"=> 5,
            "title"=> "Más",
            "season_id"=> 8
        ], [
            "id"=> 117,
            "number"=> 6,
            "title"=> "Sunset",
            "season_id"=> 8
        ], [
            "id"=> 118,
            "number"=> 7,
            "title"=> "One Minute",
            "season_id"=> 8
        ], [
            "id"=> 119,
            "number"=> 8,
            "title"=> "I See You",
            "season_id"=> 8
        ], [
            "id"=> 120,
            "number"=> 9,
            "title"=> "Kafkaesque",
            "season_id"=> 8
        ], [
            "id"=> 121,
            "number"=> 10,
            "title"=> "Fly",
            "season_id"=> 8
        ], [
            "id"=> 122,
            "number"=> 11,
            "title"=> "Abiquiu",
            "season_id"=> 8
        ], [
            "id"=> 123,
            "number"=> 12,
            "title"=> "Half Measures",
            "season_id"=> 8
        ], [
            "id"=> 124,
            "number"=> 13,
            "title"=> "Full Measure",
            "season_id"=> 8
        ], [
            "id"=> 125,
            "number"=> 1,
            "title"=> "Live Free or Die",
            "season_id"=> 10
        ], [
            "id"=> 126,
            "number"=> 2,
            "title"=> "Madrigal",
            "season_id"=> 10
        ], [
            "id"=> 127,
            "number"=> 3,
            "title"=> "Hazard Pay",
            "season_id"=> 10
        ], [
            "id"=> 128,
            "number"=> 4,
            "title"=> "Fifty-One",
            "season_id"=> 10
        ], [
            "id"=> 129,
            "number"=> 5,
            "title"=> "Dead Freight",
            "season_id"=> 10
        ], [
            "id"=> 130,
            "number"=> 6,
            "title"=> "Buyout",
            "season_id"=> 10
        ], [
            "id"=> 131,
            "number"=> 7,
            "title"=> "Say My Name",
            "season_id"=> 10
        ], [
            "id"=> 132,
            "number"=> 8,
            "title"=> "Gliding Over All",
            "season_id"=> 10
        ], [
            "id"=> 133,
            "number"=> 9,
            "title"=> "Blood Money",
            "season_id"=> 10
        ], [
            "id"=> 134,
            "number"=> 10,
            "title"=> "Buried",
            "season_id"=> 10
        ], [
            "id"=> 135,
            "number"=> 11,
            "title"=> "Confessions",
            "season_id"=> 10
        ], [
            "id"=> 136,
            "number"=> 12,
            "title"=> "Rabid Dog",
            "season_id"=> 10
        ], [
            "id"=> 137,
            "number"=> 13,
            "title"=> "To'hajiilee",
            "season_id"=> 10
        ], [
            "id"=> 138,
            "number"=> 14,
            "title"=> "Ozymandias",
            "season_id"=> 10
        ], [
            "id"=> 139,
            "number"=> 15,
            "title"=> "Granite State",
            "season_id"=> 10
        ], [
            "id"=> 140,
            "number"=> 16,
            "title"=> "Felina",
            "season_id"=> 10
        ], [
            "id"=> 141,
            "number"=> 1,
            "title"=> "Pilot",
            "season_id"=> 6
        ], [
            "id"=> 142,
            "number"=> 2,
            "title"=> "Cat's in the Bag...",
            "season_id"=> 6
        ], [
            "id"=> 143,
            "number"=> 3,
            "title"=> "...And the Bag's in the River",
            "season_id"=> 6
        ], [
            "id"=> 144,
            "number"=> 4,
            "title"=> "Cancer Man",
            "season_id"=> 6
        ], [
            "id"=> 145,
            "number"=> 5,
            "title"=> "Gray Matter",
            "season_id"=> 6
        ], [
            "id"=> 146,
            "number"=> 6,
            "title"=> "Crazy Handful of Nothin'",
            "season_id"=> 6
        ], [
            "id"=> 147,
            "number"=> 7,
            "title"=> "A No-Rough-Stuff-Type Deal",
            "season_id"=> 6
        ], [
            "id"=> 148,
            "number"=> 1,
            "title"=> "Seven Thirty-Seven",
            "season_id"=> 7
        ], [
            "id"=> 149,
            "number"=> 2,
            "title"=> "Grilled",
            "season_id"=> 7
        ], [
            "id"=> 150,
            "number"=> 3,
            "title"=> "Bit by a Dead Bee",
            "season_id"=> 7
        ], [
            "id"=> 151,
            "number"=> 4,
            "title"=> "Down",
            "season_id"=> 7
        ], [
            "id"=> 152,
            "number"=> 5,
            "title"=> "Breakage",
            "season_id"=> 7
        ], [
            "id"=> 153,
            "number"=> 6,
            "title"=> "Peekaboo",
            "season_id"=> 7
        ], [
            "id"=> 154,
            "number"=> 7,
            "title"=> "Negro y Azul",
            "season_id"=> 7
        ], [
            "id"=> 155,
            "number"=> 8,
            "title"=> "Better Call Saul",
            "season_id"=> 7
        ], [
            "id"=> 156,
            "number"=> 9,
            "title"=> "4 Days Out",
            "season_id"=> 7
        ], [
            "id"=> 157,
            "number"=> 10,
            "title"=> "Over",
            "season_id"=> 7
        ], [
            "id"=> 158,
            "number"=> 11,
            "title"=> "Mandala",
            "season_id"=> 7
        ], [
            "id"=> 159,
            "number"=> 12,
            "title"=> "Phoenix",
            "season_id"=> 7
        ], [
            "id"=> 160,
            "number"=> 13,
            "title"=> "ABQ",
            "season_id"=> 7
        ], [
            "id"=> 161,
            "number"=> 1,
            "title"=> "Box Cutter",
            "season_id"=> 9
        ], [
            "id"=> 162,
            "number"=> 2,
            "title"=> "Thirty-Eight Snub",
            "season_id"=> 9
        ], [
            "id"=> 163,
            "number"=> 3,
            "title"=> "Open House",
            "season_id"=> 9
        ], [
            "id"=> 164,
            "number"=> 4,
            "title"=> "Bullet Points",
            "season_id"=> 9
        ], [
            "id"=> 165,
            "number"=> 5,
            "title"=> "Shotgun",
            "season_id"=> 9
        ], [
            "id"=> 166,
            "number"=> 6,
            "title"=> "Cornered",
            "season_id"=> 9
        ], [
            "id"=> 167,
            "number"=> 7,
            "title"=> "Problem Dog",
            "season_id"=> 9
        ], [
            "id"=> 168,
            "number"=> 8,
            "title"=> "Hermanos",
            "season_id"=> 9
        ], [
            "id"=> 169,
            "number"=> 9,
            "title"=> "Bug",
            "season_id"=> 9
        ], [
            "id"=> 170,
            "number"=> 10,
            "title"=> "Salud",
            "season_id"=> 9
        ], [
            "id"=> 171,
            "number"=> 11,
            "title"=> "Crawl Space",
            "season_id"=> 9
        ], [
            "id"=> 172,
            "number"=> 12,
            "title"=> "End Times",
            "season_id"=> 9
        ], [
            "id"=> 173,
            "number"=> 13,
            "title"=> "Face Off",
            "season_id"=> 9
        ], [
            "id"=> 174,
            "number"=> 1,
            "title"=> "Black Tuesday",
            "season_id"=> 15
        ], [
            "id"=> 175,
            "number"=> 2,
            "title"=> "Black Cats",
            "season_id"=> 15
        ], [
            "id"=> 176,
            "number"=> 3,
            "title"=> "Strategy",
            "season_id"=> 15
        ], [
            "id"=> 177,
            "number"=> 4,
            "title"=> "The Loop",
            "season_id"=> 15
        ], [
            "id"=> 178,
            "number"=> 5,
            "title"=> "The Shock",
            "season_id"=> 15
        ], [
            "id"=> 179,
            "number"=> 6,
            "title"=> "Mr Jones",
            "season_id"=> 15
        ], [
            "id"=> 180,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 13
        ], [
            "id"=> 181,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 13
        ], [
            "id"=> 182,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 13
        ], [
            "id"=> 183,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 13
        ], [
            "id"=> 184,
            "number"=> 5,
            "title"=> "Episode 5",
            "season_id"=> 13
        ], [
            "id"=> 185,
            "number"=> 6,
            "title"=> "Episode 6",
            "season_id"=> 13
        ], [
            "id"=> 186,
            "number"=> 1,
            "title"=> "The Noose",
            "season_id"=> 14
        ], [
            "id"=> 187,
            "number"=> 2,
            "title"=> "Heathens",
            "season_id"=> 14
        ], [
            "id"=> 188,
            "number"=> 3,
            "title"=> "Blackbird",
            "season_id"=> 14
        ], [
            "id"=> 189,
            "number"=> 4,
            "title"=> "Dangerous",
            "season_id"=> 14
        ], [
            "id"=> 190,
            "number"=> 5,
            "title"=> "The Duel",
            "season_id"=> 14
        ], [
            "id"=> 191,
            "number"=> 6,
            "title"=> "The Company",
            "season_id"=> 14
        ], [
            "id"=> 192,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 12
        ], [
            "id"=> 193,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 12
        ], [
            "id"=> 194,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 12
        ], [
            "id"=> 195,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 12
        ], [
            "id"=> 196,
            "number"=> 5,
            "title"=> "Episode 5",
            "season_id"=> 12
        ], [
            "id"=> 197,
            "number"=> 6,
            "title"=> "Episode 6",
            "season_id"=> 12
        ], [
            "id"=> 198,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 11
        ], [
            "id"=> 199,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 11
        ], [
            "id"=> 200,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 11
        ], [
            "id"=> 201,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 11
        ], [
            "id"=> 202,
            "number"=> 5,
            "title"=> "Episode 5",
            "season_id"=> 11
        ], [
            "id"=> 203,
            "number"=> 6,
            "title"=> "Episode 6",
            "season_id"=> 11
        ], [
            "id"=> 204,
            "number"=> 1,
            "title"=> "Dragonstone",
            "season_id"=> 22
        ], [
            "id"=> 205,
            "number"=> 2,
            "title"=> "Stormborn",
            "season_id"=> 22
        ], [
            "id"=> 206,
            "number"=> 3,
            "title"=> "The Queen's Justice",
            "season_id"=> 22
        ], [
            "id"=> 207,
            "number"=> 4,
            "title"=> "The Spoils of War",
            "season_id"=> 22
        ], [
            "id"=> 208,
            "number"=> 5,
            "title"=> "Eastwatch",
            "season_id"=> 22
        ], [
            "id"=> 209,
            "number"=> 6,
            "title"=> "Beyond the Wall",
            "season_id"=> 22
        ], [
            "id"=> 210,
            "number"=> 7,
            "title"=> "The Dragon and the Wolf",
            "season_id"=> 22
        ], [
            "id"=> 211,
            "number"=> 1,
            "title"=> "Valar Dohaeris",
            "season_id"=> 18
        ], [
            "id"=> 212,
            "number"=> 2,
            "title"=> "Dark Wings, Dark Words",
            "season_id"=> 18
        ], [
            "id"=> 213,
            "number"=> 3,
            "title"=> "Walk of Punishment",
            "season_id"=> 18
        ], [
            "id"=> 214,
            "number"=> 4,
            "title"=> "And Now His Watch Is Ended",
            "season_id"=> 18
        ], [
            "id"=> 215,
            "number"=> 5,
            "title"=> "Kissed by Fire",
            "season_id"=> 18
        ], [
            "id"=> 216,
            "number"=> 6,
            "title"=> "The Climb",
            "season_id"=> 18
        ], [
            "id"=> 217,
            "number"=> 7,
            "title"=> "The Bear and the Maiden Fair",
            "season_id"=> 18
        ], [
            "id"=> 218,
            "number"=> 8,
            "title"=> "Second Sons",
            "season_id"=> 18
        ], [
            "id"=> 219,
            "number"=> 9,
            "title"=> "The Rains of Castamere",
            "season_id"=> 18
        ], [
            "id"=> 220,
            "number"=> 10,
            "title"=> "Mhysa",
            "season_id"=> 18
        ], [
            "id"=> 221,
            "number"=> 1,
            "title"=> "The Red Woman",
            "season_id"=> 21
        ], [
            "id"=> 222,
            "number"=> 2,
            "title"=> "Home",
            "season_id"=> 21
        ], [
            "id"=> 223,
            "number"=> 3,
            "title"=> "Oathbreaker",
            "season_id"=> 21
        ], [
            "id"=> 224,
            "number"=> 4,
            "title"=> "Book of the Stranger",
            "season_id"=> 21
        ], [
            "id"=> 225,
            "number"=> 5,
            "title"=> "The Door",
            "season_id"=> 21
        ], [
            "id"=> 226,
            "number"=> 6,
            "title"=> "Blood of My Blood",
            "season_id"=> 21
        ], [
            "id"=> 227,
            "number"=> 7,
            "title"=> "The Broken Man",
            "season_id"=> 21
        ], [
            "id"=> 228,
            "number"=> 8,
            "title"=> "No One",
            "season_id"=> 21
        ], [
            "id"=> 229,
            "number"=> 9,
            "title"=> "Battle of the Bastards",
            "season_id"=> 21
        ], [
            "id"=> 230,
            "number"=> 10,
            "title"=> "The Winds of Winter",
            "season_id"=> 21
        ], [
            "id"=> 231,
            "number"=> 1,
            "title"=> "The North Remembers",
            "season_id"=> 17
        ], [
            "id"=> 232,
            "number"=> 2,
            "title"=> "The Night Lands",
            "season_id"=> 17
        ], [
            "id"=> 233,
            "number"=> 3,
            "title"=> "What is Dead May Never Die",
            "season_id"=> 17
        ], [
            "id"=> 234,
            "number"=> 4,
            "title"=> "Garden of Bones",
            "season_id"=> 17
        ], [
            "id"=> 235,
            "number"=> 5,
            "title"=> "The Ghost of Harrenhal",
            "season_id"=> 17
        ], [
            "id"=> 236,
            "number"=> 6,
            "title"=> "The Old Gods and the New",
            "season_id"=> 17
        ], [
            "id"=> 237,
            "number"=> 7,
            "title"=> "A Man Without Honor",
            "season_id"=> 17
        ], [
            "id"=> 238,
            "number"=> 8,
            "title"=> "The Prince of Winterfell",
            "season_id"=> 17
        ], [
            "id"=> 239,
            "number"=> 9,
            "title"=> "Blackwater",
            "season_id"=> 17
        ], [
            "id"=> 240,
            "number"=> 10,
            "title"=> "Valar Morghulis",
            "season_id"=> 17
        ], [
            "id"=> 241,
            "number"=> 1,
            "title"=> "The Wars to Come",
            "season_id"=> 20
        ], [
            "id"=> 242,
            "number"=> 2,
            "title"=> "The House of Black and White",
            "season_id"=> 20
        ], [
            "id"=> 243,
            "number"=> 3,
            "title"=> "High Sparrow",
            "season_id"=> 20
        ], [
            "id"=> 244,
            "number"=> 4,
            "title"=> "Sons of the Harpy",
            "season_id"=> 20
        ], [
            "id"=> 245,
            "number"=> 5,
            "title"=> "Kill the Boy",
            "season_id"=> 20
        ], [
            "id"=> 246,
            "number"=> 6,
            "title"=> "Unbowed, Unbent, Unbroken",
            "season_id"=> 20
        ], [
            "id"=> 247,
            "number"=> 7,
            "title"=> "The Gift",
            "season_id"=> 20
        ], [
            "id"=> 248,
            "number"=> 8,
            "title"=> "Hardhome",
            "season_id"=> 20
        ], [
            "id"=> 249,
            "number"=> 9,
            "title"=> "The Dance of Dragons",
            "season_id"=> 20
        ], [
            "id"=> 250,
            "number"=> 10,
            "title"=> "Mother's Mercy",
            "season_id"=> 20
        ], [
            "id"=> 251,
            "number"=> 1,
            "title"=> "Winter Is Coming",
            "season_id"=> 16
        ], [
            "id"=> 252,
            "number"=> 2,
            "title"=> "The Kingsroad",
            "season_id"=> 16
        ], [
            "id"=> 253,
            "number"=> 3,
            "title"=> "Lord Snow",
            "season_id"=> 16
        ], [
            "id"=> 254,
            "number"=> 4,
            "title"=> "Cripples, Bastards, and Broken Things",
            "season_id"=> 16
        ], [
            "id"=> 255,
            "number"=> 5,
            "title"=> "The Wolf and the Lion",
            "season_id"=> 16
        ], [
            "id"=> 256,
            "number"=> 6,
            "title"=> "A Golden Crown",
            "season_id"=> 16
        ], [
            "id"=> 257,
            "number"=> 7,
            "title"=> "You Win or You Die",
            "season_id"=> 16
        ], [
            "id"=> 258,
            "number"=> 8,
            "title"=> "The Pointy End",
            "season_id"=> 16
        ], [
            "id"=> 259,
            "number"=> 9,
            "title"=> "Baelor",
            "season_id"=> 16
        ], [
            "id"=> 260,
            "number"=> 10,
            "title"=> "Fire and Blood",
            "season_id"=> 16
        ], [
            "id"=> 261,
            "number"=> 1,
            "title"=> "Two Swords",
            "season_id"=> 19
        ], [
            "id"=> 262,
            "number"=> 2,
            "title"=> "The Lion and the Rose",
            "season_id"=> 19
        ], [
            "id"=> 263,
            "number"=> 3,
            "title"=> "Breaker of Chains",
            "season_id"=> 19
        ], [
            "id"=> 264,
            "number"=> 4,
            "title"=> "Oathkeeper",
            "season_id"=> 19
        ], [
            "id"=> 265,
            "number"=> 5,
            "title"=> "First of His Name",
            "season_id"=> 19
        ], [
            "id"=> 266,
            "number"=> 6,
            "title"=> "The Laws of Gods and Men",
            "season_id"=> 19
        ], [
            "id"=> 267,
            "number"=> 7,
            "title"=> "Mockingbird",
            "season_id"=> 19
        ], [
            "id"=> 268,
            "number"=> 8,
            "title"=> "The Mountain and the Viper",
            "season_id"=> 19
        ], [
            "id"=> 269,
            "number"=> 9,
            "title"=> "The Watchers on the Wall",
            "season_id"=> 19
        ], [
            "id"=> 270,
            "number"=> 10,
            "title"=> "The Children",
            "season_id"=> 19
        ], [
            "id"=> 271,
            "number"=> 1,
            "title"=> "Winterfell",
            "season_id"=> 23
        ], [
            "id"=> 272,
            "number"=> 2,
            "title"=> "A Knight of the Seven Kingdoms",
            "season_id"=> 23
        ], [
            "id"=> 273,
            "number"=> 3,
            "title"=> "The Long Night",
            "season_id"=> 23
        ], [
            "id"=> 274,
            "number"=> 4,
            "title"=> "The Last of the Starks",
            "season_id"=> 23
        ], [
            "id"=> 275,
            "number"=> 5,
            "title"=> "The Bells",
            "season_id"=> 23
        ], [
            "id"=> 276,
            "number"=> 6,
            "title"=> "The Iron Throne",
            "season_id"=> 23
        ], [
            "id"=> 277,
            "number"=> 1,
            "title"=> "Mercenary",
            "season_id"=> 26
        ], [
            "id"=> 278,
            "number"=> 2,
            "title"=> "The Wanderer",
            "season_id"=> 26
        ], [
            "id"=> 279,
            "number"=> 3,
            "title"=> "Warrior's Fate",
            "season_id"=> 26
        ], [
            "id"=> 280,
            "number"=> 4,
            "title"=> "Scarred",
            "season_id"=> 26
        ], [
            "id"=> 281,
            "number"=> 5,
            "title"=> "The Usurper",
            "season_id"=> 26
        ], [
            "id"=> 282,
            "number"=> 6,
            "title"=> "Born Again",
            "season_id"=> 26
        ], [
            "id"=> 283,
            "number"=> 7,
            "title"=> "Paris",
            "season_id"=> 26
        ], [
            "id"=> 284,
            "number"=> 8,
            "title"=> "To the Gates!",
            "season_id"=> 26
        ], [
            "id"=> 285,
            "number"=> 9,
            "title"=> "Breaking Point",
            "season_id"=> 26
        ], [
            "id"=> 286,
            "number"=> 10,
            "title"=> "The Dead",
            "season_id"=> 26
        ], [
            "id"=> 287,
            "number"=> 1,
            "title"=> "A Good Treason",
            "season_id"=> 27
        ], [
            "id"=> 288,
            "number"=> 2,
            "title"=> "Kill the Queen",
            "season_id"=> 27
        ], [
            "id"=> 289,
            "number"=> 3,
            "title"=> "Mercy",
            "season_id"=> 27
        ], [
            "id"=> 290,
            "number"=> 4,
            "title"=> "Yol",
            "season_id"=> 27
        ], [
            "id"=> 291,
            "number"=> 5,
            "title"=> "Promised",
            "season_id"=> 27
        ], [
            "id"=> 292,
            "number"=> 6,
            "title"=> "What Might Have Been",
            "season_id"=> 27
        ], [
            "id"=> 293,
            "number"=> 7,
            "title"=> "The Profit and the Loss",
            "season_id"=> 27
        ], [
            "id"=> 294,
            "number"=> 8,
            "title"=> "Portage",
            "season_id"=> 27
        ], [
            "id"=> 295,
            "number"=> 9,
            "title"=> "Death All 'Round",
            "season_id"=> 27
        ], [
            "id"=> 296,
            "number"=> 10,
            "title"=> "The Last Ship",
            "season_id"=> 27
        ], [
            "id"=> 297,
            "number"=> 11,
            "title"=> "The Outsider",
            "season_id"=> 27
        ], [
            "id"=> 298,
            "number"=> 12,
            "title"=> "The Vision",
            "season_id"=> 27
        ], [
            "id"=> 299,
            "number"=> 13,
            "title"=> "Two Journeys",
            "season_id"=> 27
        ], [
            "id"=> 300,
            "number"=> 14,
            "title"=> "In the Uncertain Hour Before the Morning",
            "season_id"=> 27
        ], [
            "id"=> 301,
            "number"=> 15,
            "title"=> "All His Angels",
            "season_id"=> 27
        ], [
            "id"=> 302,
            "number"=> 16,
            "title"=> "Crossings",
            "season_id"=> 27
        ], [
            "id"=> 303,
            "number"=> 17,
            "title"=> "The Great Army",
            "season_id"=> 27
        ], [
            "id"=> 304,
            "number"=> 18,
            "title"=> "Revenge",
            "season_id"=> 27
        ], [
            "id"=> 305,
            "number"=> 19,
            "title"=> "On the Eve",
            "season_id"=> 27
        ], [
            "id"=> 306,
            "number"=> 20,
            "title"=> "The Reckoning",
            "season_id"=> 27
        ], [
            "id"=> 307,
            "number"=> 1,
            "title"=> "Brother's War",
            "season_id"=> 25
        ], [
            "id"=> 308,
            "number"=> 2,
            "title"=> "Invasion",
            "season_id"=> 25
        ], [
            "id"=> 309,
            "number"=> 3,
            "title"=> "Treachery",
            "season_id"=> 25
        ], [
            "id"=> 310,
            "number"=> 4,
            "title"=> "Eye for an Eye",
            "season_id"=> 25
        ], [
            "id"=> 311,
            "number"=> 5,
            "title"=> "Answers in Blood",
            "season_id"=> 25
        ], [
            "id"=> 312,
            "number"=> 6,
            "title"=> "Unforgiven",
            "season_id"=> 25
        ], [
            "id"=> 313,
            "number"=> 7,
            "title"=> "Blood Eagle",
            "season_id"=> 25
        ], [
            "id"=> 314,
            "number"=> 8,
            "title"=> "Boneless",
            "season_id"=> 25
        ], [
            "id"=> 315,
            "number"=> 9,
            "title"=> "The Choice",
            "season_id"=> 25
        ], [
            "id"=> 316,
            "number"=> 10,
            "title"=> "The Lord's Prayer",
            "season_id"=> 25
        ], [
            "id"=> 317,
            "number"=> 1,
            "title"=> "The Departed Part One",
            "season_id"=> 28
        ], [
            "id"=> 318,
            "number"=> 2,
            "title"=> "The Departed Part Two",
            "season_id"=> 28
        ], [
            "id"=> 319,
            "number"=> 3,
            "title"=> "Homeland",
            "season_id"=> 28
        ], [
            "id"=> 320,
            "number"=> 4,
            "title"=> "The Plan",
            "season_id"=> 28
        ], [
            "id"=> 321,
            "number"=> 5,
            "title"=> "The Prisoner",
            "season_id"=> 28
        ], [
            "id"=> 322,
            "number"=> 6,
            "title"=> "The Message",
            "season_id"=> 28
        ], [
            "id"=> 323,
            "number"=> 7,
            "title"=> "Full Moon",
            "season_id"=> 28
        ], [
            "id"=> 324,
            "number"=> 8,
            "title"=> "The Joke",
            "season_id"=> 28
        ], [
            "id"=> 325,
            "number"=> 9,
            "title"=> "A Simple Story",
            "season_id"=> 28
        ], [
            "id"=> 326,
            "number"=> 10,
            "title"=> "Moments of Vision",
            "season_id"=> 28
        ], [
            "id"=> 327,
            "number"=> 11,
            "title"=> "The Revelation",
            "season_id"=> 28
        ], [
            "id"=> 328,
            "number"=> 12,
            "title"=> "Murder Most Foul",
            "season_id"=> 28
        ], [
            "id"=> 329,
            "number"=> 13,
            "title"=> "A New God",
            "season_id"=> 28
        ], [
            "id"=> 330,
            "number"=> 14,
            "title"=> "The Lost Moment",
            "season_id"=> 28
        ], [
            "id"=> 331,
            "number"=> 15,
            "title"=> "Hell",
            "season_id"=> 28
        ], [
            "id"=> 332,
            "number"=> 16,
            "title"=> "The Buddha",
            "season_id"=> 28
        ], [
            "id"=> 333,
            "number"=> 17,
            "title"=> "The Most Terrible Thing",
            "season_id"=> 28
        ], [
            "id"=> 334,
            "number"=> 18,
            "title"=> "Baldur",
            "season_id"=> 28
        ], [
            "id"=> 335,
            "number"=> 19,
            "title"=> "What Happens in the Cave",
            "season_id"=> 28
        ], [
            "id"=> 336,
            "number"=> 20,
            "title"=> "Ragnarok",
            "season_id"=> 28
        ], [
            "id"=> 337,
            "number"=> 1,
            "title"=> "New Beginnings",
            "season_id"=> 29
        ], [
            "id"=> 338,
            "number"=> 2,
            "title"=> "The Prophet",
            "season_id"=> 29
        ], [
            "id"=> 339,
            "number"=> 3,
            "title"=> "Ghosts, Gods, and Running Dogs",
            "season_id"=> 29
        ], [
            "id"=> 340,
            "number"=> 4,
            "title"=> "All the Prisoners",
            "season_id"=> 29
        ], [
            "id"=> 341,
            "number"=> 5,
            "title"=> "The Key",
            "season_id"=> 29
        ], [
            "id"=> 342,
            "number"=> 6,
            "title"=> "Death and the Serpent",
            "season_id"=> 29
        ], [
            "id"=> 343,
            "number"=> 7,
            "title"=> "The Ice Maiden",
            "season_id"=> 29
        ], [
            "id"=> 344,
            "number"=> 8,
            "title"=> "Valhalla Can Wait",
            "season_id"=> 29
        ], [
            "id"=> 345,
            "number"=> 9,
            "title"=> "Resurrection",
            "season_id"=> 29
        ], [
            "id"=> 346,
            "number"=> 10,
            "title"=> "The Best Laid Plans",
            "season_id"=> 29
        ], [
            "id"=> 347,
            "number"=> 1,
            "title"=> "Rites of Passage",
            "season_id"=> 24
        ], [
            "id"=> 348,
            "number"=> 2,
            "title"=> "Wrath of the Northmen",
            "season_id"=> 24
        ], [
            "id"=> 349,
            "number"=> 3,
            "title"=> "Dispossessed",
            "season_id"=> 24
        ], [
            "id"=> 350,
            "number"=> 4,
            "title"=> "Trial",
            "season_id"=> 24
        ], [
            "id"=> 351,
            "number"=> 5,
            "title"=> "Raid",
            "season_id"=> 24
        ], [
            "id"=> 352,
            "number"=> 6,
            "title"=> "Burial of the Dead",
            "season_id"=> 24
        ], [
            "id"=> 353,
            "number"=> 7,
            "title"=> "A King's Ransom",
            "season_id"=> 24
        ], [
            "id"=> 354,
            "number"=> 8,
            "title"=> "Sacrifice",
            "season_id"=> 24
        ], [
            "id"=> 355,
            "number"=> 9,
            "title"=> "All Change",
            "season_id"=> 24
        ], [
            "id"=> 356,
            "number"=> 1,
            "title"=> "Not Your Average Joe",
            "season_id"=> 30
        ], [
            "id"=> 357,
            "number"=> 2,
            "title"=> "Cult of Personality",
            "season_id"=> 30
        ], [
            "id"=> 358,
            "number"=> 3,
            "title"=> "The Secret",
            "season_id"=> 30
        ], [
            "id"=> 359,
            "number"=> 4,
            "title"=> "Playing with Fire",
            "season_id"=> 30
        ], [
            "id"=> 360,
            "number"=> 5,
            "title"=> "Make America Exotic Again",
            "season_id"=> 30
        ], [
            "id"=> 361,
            "number"=> 6,
            "title"=> "The Noble Thing to Do",
            "season_id"=> 30
        ], [
            "id"=> 362,
            "number"=> 7,
            "title"=> "Dethroned",
            "season_id"=> 30
        ], [
            "id"=> 363,
            "number"=> 8,
            "title"=> "The Tiger King and I",
            "season_id"=> 30
        ], [
            "id"=> 364,
            "number"=> 1,
            "title"=> "Everything Except Poison",
            "season_id"=> 31
        ], [
            "id"=> 365,
            "number"=> 2,
            "title"=> "Make It Strawberry",
            "season_id"=> 31
        ], [
            "id"=> 366,
            "number"=> 3,
            "title"=> "War in Waco",
            "season_id"=> 31
        ], [
            "id"=> 367,
            "number"=> 4,
            "title"=> "All the Damn Lies",
            "season_id"=> 31
        ], [
            "id"=> 368,
            "number"=> 5,
            "title"=> "Not One Shred of Evidence",
            "season_id"=> 31
        ], [
            "id"=> 369,
            "number"=> 1,
            "title"=> "Cat and Mouse",
            "season_id"=> 32
        ], [
            "id"=> 370,
            "number"=> 2,
            "title"=> "Killing for Clicks",
            "season_id"=> 32
        ], [
            "id"=> 371,
            "number"=> 3,
            "title"=> "Closing the Net",
            "season_id"=> 32
        ], [
            "id"=> 372,
            "number"=> 1,
            "title"=> "Scylla",
            "season_id"=> 36
        ], [
            "id"=> 373,
            "number"=> 2,
            "title"=> "Breaking & Entering",
            "season_id"=> 36
        ], [
            "id"=> 374,
            "number"=> 3,
            "title"=> "Shut Down",
            "season_id"=> 36
        ], [
            "id"=> 375,
            "number"=> 4,
            "title"=> "Eagles & Angels",
            "season_id"=> 36
        ], [
            "id"=> 376,
            "number"=> 5,
            "title"=> "Safe & Sound",
            "season_id"=> 36
        ], [
            "id"=> 377,
            "number"=> 6,
            "title"=> "Blow Out",
            "season_id"=> 36
        ], [
            "id"=> 378,
            "number"=> 7,
            "title"=> "Five the Hard Way",
            "season_id"=> 36
        ], [
            "id"=> 379,
            "number"=> 8,
            "title"=> "The Price",
            "season_id"=> 36
        ], [
            "id"=> 380,
            "number"=> 9,
            "title"=> "Greatness Achieved",
            "season_id"=> 36
        ], [
            "id"=> 381,
            "number"=> 10,
            "title"=> "The Legend",
            "season_id"=> 36
        ], [
            "id"=> 382,
            "number"=> 11,
            "title"=> "Quiet Riot",
            "season_id"=> 36
        ], [
            "id"=> 383,
            "number"=> 12,
            "title"=> "Selfless",
            "season_id"=> 36
        ], [
            "id"=> 384,
            "number"=> 13,
            "title"=> "Deal Or No Deal",
            "season_id"=> 36
        ], [
            "id"=> 385,
            "number"=> 14,
            "title"=> "Just Business",
            "season_id"=> 36
        ], [
            "id"=> 386,
            "number"=> 15,
            "title"=> "Going Under",
            "season_id"=> 36
        ], [
            "id"=> 387,
            "number"=> 16,
            "title"=> "The Sunshine State",
            "season_id"=> 36
        ], [
            "id"=> 388,
            "number"=> 17,
            "title"=> "The Mother Lode",
            "season_id"=> 36
        ], [
            "id"=> 389,
            "number"=> 18,
            "title"=> "VS.",
            "season_id"=> 36
        ], [
            "id"=> 390,
            "number"=> 19,
            "title"=> "S.O.B",
            "season_id"=> 36
        ], [
            "id"=> 391,
            "number"=> 20,
            "title"=> "Cowboys & Indians",
            "season_id"=> 36
        ], [
            "id"=> 392,
            "number"=> 21,
            "title"=> "Rate of Exchange",
            "season_id"=> 36
        ], [
            "id"=> 393,
            "number"=> 22,
            "title"=> "Killing Your Number",
            "season_id"=> 36
        ], [
            "id"=> 394,
            "number"=> 1,
            "title"=> "Manhunt",
            "season_id"=> 34
        ], [
            "id"=> 395,
            "number"=> 2,
            "title"=> "Otis",
            "season_id"=> 34
        ], [
            "id"=> 396,
            "number"=> 3,
            "title"=> "Scan",
            "season_id"=> 34
        ], [
            "id"=> 397,
            "number"=> 4,
            "title"=> "First Down",
            "season_id"=> 34
        ], [
            "id"=> 398,
            "number"=> 5,
            "title"=> "Map 1213",
            "season_id"=> 34
        ], [
            "id"=> 399,
            "number"=> 6,
            "title"=> "Subdivision",
            "season_id"=> 34
        ], [
            "id"=> 400,
            "number"=> 7,
            "title"=> "Buried",
            "season_id"=> 34
        ], [
            "id"=> 401,
            "number"=> 8,
            "title"=> "Dead Fall",
            "season_id"=> 34
        ], [
            "id"=> 402,
            "number"=> 9,
            "title"=> "Unearthed",
            "season_id"=> 34
        ], [
            "id"=> 403,
            "number"=> 10,
            "title"=> "Rendezvous",
            "season_id"=> 34
        ], [
            "id"=> 404,
            "number"=> 11,
            "title"=> "Bolshoi Booze",
            "season_id"=> 34
        ], [
            "id"=> 405,
            "number"=> 12,
            "title"=> "Disconnect",
            "season_id"=> 34
        ], [
            "id"=> 406,
            "number"=> 13,
            "title"=> "The Killing Box",
            "season_id"=> 34
        ], [
            "id"=> 407,
            "number"=> 14,
            "title"=> "John Doe",
            "season_id"=> 34
        ], [
            "id"=> 408,
            "number"=> 15,
            "title"=> "The Message",
            "season_id"=> 34
        ], [
            "id"=> 409,
            "number"=> 16,
            "title"=> "Chicago",
            "season_id"=> 34
        ], [
            "id"=> 410,
            "number"=> 17,
            "title"=> "Bad Blood",
            "season_id"=> 34
        ], [
            "id"=> 411,
            "number"=> 18,
            "title"=> "Wash",
            "season_id"=> 34
        ], [
            "id"=> 412,
            "number"=> 19,
            "title"=> "Sweet Caroline",
            "season_id"=> 34
        ], [
            "id"=> 413,
            "number"=> 20,
            "title"=> "Panama",
            "season_id"=> 34
        ], [
            "id"=> 414,
            "number"=> 21,
            "title"=> "Fin Del Camino",
            "season_id"=> 34
        ], [
            "id"=> 415,
            "number"=> 22,
            "title"=> "Sona",
            "season_id"=> 34
        ], [
            "id"=> 416,
            "number"=> 1,
            "title"=> "Ogygia",
            "season_id"=> 37
        ], [
            "id"=> 417,
            "number"=> 2,
            "title"=> "Kaniel Outis",
            "season_id"=> 37
        ], [
            "id"=> 418,
            "number"=> 3,
            "title"=> "The Liar",
            "season_id"=> 37
        ], [
            "id"=> 419,
            "number"=> 4,
            "title"=> "The Prisoner's Dilemma",
            "season_id"=> 37
        ], [
            "id"=> 420,
            "number"=> 5,
            "title"=> "Contingency",
            "season_id"=> 37
        ], [
            "id"=> 421,
            "number"=> 6,
            "title"=> "Phaeacia",
            "season_id"=> 37
        ], [
            "id"=> 422,
            "number"=> 7,
            "title"=> "Wine-Dark Sea",
            "season_id"=> 37
        ], [
            "id"=> 423,
            "number"=> 8,
            "title"=> "Progeny",
            "season_id"=> 37
        ], [
            "id"=> 424,
            "number"=> 9,
            "title"=> "Behind the Eyes",
            "season_id"=> 37
        ], [
            "id"=> 425,
            "number"=> 1,
            "title"=> "Orientación",
            "season_id"=> 35
        ], [
            "id"=> 426,
            "number"=> 2,
            "title"=> "Fire/Water",
            "season_id"=> 35
        ], [
            "id"=> 427,
            "number"=> 3,
            "title"=> "Call Waiting",
            "season_id"=> 35
        ], [
            "id"=> 428,
            "number"=> 4,
            "title"=> "Good Fences",
            "season_id"=> 35
        ], [
            "id"=> 429,
            "number"=> 5,
            "title"=> "Interference",
            "season_id"=> 35
        ], [
            "id"=> 430,
            "number"=> 6,
            "title"=> "Photo Finish",
            "season_id"=> 35
        ], [
            "id"=> 431,
            "number"=> 7,
            "title"=> "Vamonos",
            "season_id"=> 35
        ], [
            "id"=> 432,
            "number"=> 8,
            "title"=> "Bang and Burn",
            "season_id"=> 35
        ], [
            "id"=> 433,
            "number"=> 9,
            "title"=> "Boxed In",
            "season_id"=> 35
        ], [
            "id"=> 434,
            "number"=> 10,
            "title"=> "Dirt Nap",
            "season_id"=> 35
        ], [
            "id"=> 435,
            "number"=> 11,
            "title"=> "Under and Out",
            "season_id"=> 35
        ], [
            "id"=> 436,
            "number"=> 12,
            "title"=> "Hell or High Water",
            "season_id"=> 35
        ], [
            "id"=> 437,
            "number"=> 13,
            "title"=> "The Art of the Deal",
            "season_id"=> 35
        ], [
            "id"=> 438,
            "number"=> 1,
            "title"=> "Pilot",
            "season_id"=> 33
        ], [
            "id"=> 439,
            "number"=> 2,
            "title"=> "Allen",
            "season_id"=> 33
        ], [
            "id"=> 440,
            "number"=> 3,
            "title"=> "Cell Test",
            "season_id"=> 33
        ], [
            "id"=> 441,
            "number"=> 4,
            "title"=> "Cute Poison",
            "season_id"=> 33
        ], [
            "id"=> 442,
            "number"=> 5,
            "title"=> "English, Fitz or Percy",
            "season_id"=> 33
        ], [
            "id"=> 443,
            "number"=> 6,
            "title"=> "Riots, Drills and the Devil (Part 1)",
            "season_id"=> 33
        ], [
            "id"=> 444,
            "number"=> 7,
            "title"=> "Riots, Drills and the Devil (Part 2)",
            "season_id"=> 33
        ], [
            "id"=> 445,
            "number"=> 8,
            "title"=> "The Old Head",
            "season_id"=> 33
        ], [
            "id"=> 446,
            "number"=> 9,
            "title"=> "Tweener",
            "season_id"=> 33
        ], [
            "id"=> 447,
            "number"=> 10,
            "title"=> "Sleight of Hand",
            "season_id"=> 33
        ], [
            "id"=> 448,
            "number"=> 11,
            "title"=> "And Then There Were 7",
            "season_id"=> 33
        ], [
            "id"=> 449,
            "number"=> 12,
            "title"=> "Odd Man Out",
            "season_id"=> 33
        ], [
            "id"=> 450,
            "number"=> 13,
            "title"=> "End of the Tunnel",
            "season_id"=> 33
        ], [
            "id"=> 451,
            "number"=> 14,
            "title"=> "The Rat",
            "season_id"=> 33
        ], [
            "id"=> 452,
            "number"=> 15,
            "title"=> "By the Skin and the Teeth",
            "season_id"=> 33
        ], [
            "id"=> 453,
            "number"=> 16,
            "title"=> "Brother's Keeper",
            "season_id"=> 33
        ], [
            "id"=> 454,
            "number"=> 17,
            "title"=> "J-Cat",
            "season_id"=> 33
        ], [
            "id"=> 455,
            "number"=> 18,
            "title"=> "Bluff",
            "season_id"=> 33
        ], [
            "id"=> 456,
            "number"=> 19,
            "title"=> "The Key",
            "season_id"=> 33
        ], [
            "id"=> 457,
            "number"=> 20,
            "title"=> "Tonight",
            "season_id"=> 33
        ], [
            "id"=> 458,
            "number"=> 21,
            "title"=> "Go",
            "season_id"=> 33
        ], [
            "id"=> 459,
            "number"=> 22,
            "title"=> "Flight",
            "season_id"=> 33
        ], [
            "id"=> 460,
            "number"=> 1,
            "title"=> "Descenso",
            "season_id"=> 38
        ], [
            "id"=> 461,
            "number"=> 2,
            "title"=> "The Sword of Simón Bolivar",
            "season_id"=> 38
        ], [
            "id"=> 462,
            "number"=> 3,
            "title"=> "The Men of Always",
            "season_id"=> 38
        ], [
            "id"=> 463,
            "number"=> 4,
            "title"=> "The Palace in Flames",
            "season_id"=> 38
        ], [
            "id"=> 464,
            "number"=> 5,
            "title"=> "There Will Be a Future",
            "season_id"=> 38
        ], [
            "id"=> 465,
            "number"=> 6,
            "title"=> "Explosivos",
            "season_id"=> 38
        ], [
            "id"=> 466,
            "number"=> 7,
            "title"=> "You Will Cry Tears of Blood",
            "season_id"=> 38
        ], [
            "id"=> 467,
            "number"=> 8,
            "title"=> "La Gran Mentira",
            "season_id"=> 38
        ], [
            "id"=> 468,
            "number"=> 9,
            "title"=> "La Catedral",
            "season_id"=> 38
        ], [
            "id"=> 469,
            "number"=> 10,
            "title"=> "Despegue",
            "season_id"=> 38
        ], [
            "id"=> 470,
            "number"=> 1,
            "title"=> "The Kingpin Strategy",
            "season_id"=> 40
        ], [
            "id"=> 471,
            "number"=> 2,
            "title"=> "The Cali KGB",
            "season_id"=> 40
        ], [
            "id"=> 472,
            "number"=> 3,
            "title"=> "Follow the Money",
            "season_id"=> 40
        ], [
            "id"=> 473,
            "number"=> 4,
            "title"=> "Checkmate",
            "season_id"=> 40
        ], [
            "id"=> 474,
            "number"=> 5,
            "title"=> "MRO",
            "season_id"=> 40
        ], [
            "id"=> 475,
            "number"=> 6,
            "title"=> "Best Laid Plans",
            "season_id"=> 40
        ], [
            "id"=> 476,
            "number"=> 7,
            "title"=> "Sin Salida",
            "season_id"=> 40
        ], [
            "id"=> 477,
            "number"=> 8,
            "title"=> "Convivir",
            "season_id"=> 40
        ], [
            "id"=> 478,
            "number"=> 9,
            "title"=> "Todos Los Hombres del Presidente",
            "season_id"=> 40
        ], [
            "id"=> 479,
            "number"=> 10,
            "title"=> "Going Back to Cali",
            "season_id"=> 40
        ], [
            "id"=> 480,
            "number"=> 1,
            "title"=> "Free at Last",
            "season_id"=> 39
        ], [
            "id"=> 481,
            "number"=> 2,
            "title"=> "Cambalache",
            "season_id"=> 39
        ], [
            "id"=> 482,
            "number"=> 3,
            "title"=> "Our Man in Madrid",
            "season_id"=> 39
        ], [
            "id"=> 483,
            "number"=> 4,
            "title"=> "The Good, The Bad, and The Dead",
            "season_id"=> 39
        ], [
            "id"=> 484,
            "number"=> 5,
            "title"=> "The Enemies of My Enemy",
            "season_id"=> 39
        ], [
            "id"=> 485,
            "number"=> 6,
            "title"=> "Los Pepes",
            "season_id"=> 39
        ], [
            "id"=> 486,
            "number"=> 7,
            "title"=> "Deutschland 93",
            "season_id"=> 39
        ], [
            "id"=> 487,
            "number"=> 8,
            "title"=> "Exit El Patrón",
            "season_id"=> 39
        ], [
            "id"=> 488,
            "number"=> 9,
            "title"=> "Neuestra Finca",
            "season_id"=> 39
        ], [
            "id"=> 489,
            "number"=> 10,
            "title"=> "Al Fin Cayó!",
            "season_id"=> 39
        ], [
            "id"=> 490,
            "number"=> 1,
            "title"=> "Encouragement",
            "season_id"=> 41
        ], [
            "id"=> 491,
            "number"=> 2,
            "title"=> "House Party",
            "season_id"=> 41
        ], [
            "id"=> 492,
            "number"=> 3,
            "title"=> "Liftoff",
            "season_id"=> 41
        ], [
            "id"=> 493,
            "number"=> 4,
            "title"=> "Crush",
            "season_id"=> 41
        ], [
            "id"=> 494,
            "number"=> 5,
            "title"=> "Mean Girl",
            "season_id"=> 41
        ], [
            "id"=> 495,
            "number"=> 6,
            "title"=> "Issues",
            "season_id"=> 41
        ], [
            "id"=> 496,
            "number"=> 7,
            "title"=> "Big Dance",
            "season_id"=> 41
        ], [
            "id"=> 497,
            "number"=> 8,
            "title"=> "Violated",
            "season_id"=> 41
        ], [
            "id"=> 498,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 42
        ], [
            "id"=> 499,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 42
        ], [
            "id"=> 500,
            "number"=> 3,
            "title"=> "Episode 3",
            "season_id"=> 42
        ], [
            "id"=> 501,
            "number"=> 4,
            "title"=> "Episode 4",
            "season_id"=> 42
        ], [
            "id"=> 502,
            "number"=> 5,
            "title"=> "Episode 5",
            "season_id"=> 42
        ], [
            "id"=> 503,
            "number"=> 6,
            "title"=> "Episode 6",
            "season_id"=> 42
        ], [
            "id"=> 504,
            "number"=> 7,
            "title"=> "Episode 7",
            "season_id"=> 42
        ], [
            "id"=> 505,
            "number"=> 8,
            "title"=> "Episode 8",
            "season_id"=> 42
        ], [
            "id"=> 506,
            "number"=> 1,
            "title"=> "All to Play For",
            "season_id"=> 43
        ], [
            "id"=> 507,
            "number"=> 2,
            "title"=> "The King of Spain",
            "season_id"=> 43
        ], [
            "id"=> 508,
            "number"=> 3,
            "title"=> "Redemption",
            "season_id"=> 43
        ], [
            "id"=> 509,
            "number"=> 4,
            "title"=> "The Art of War",
            "season_id"=> 43
        ], [
            "id"=> 510,
            "number"=> 5,
            "title"=> "Trouble at the Top",
            "season_id"=> 43
        ], [
            "id"=> 511,
            "number"=> 6,
            "title"=> "All or Nothing",
            "season_id"=> 43
        ], [
            "id"=> 512,
            "number"=> 7,
            "title"=> "Keeping Your Head",
            "season_id"=> 43
        ], [
            "id"=> 513,
            "number"=> 8,
            "title"=> "The Next Generation",
            "season_id"=> 43
        ], [
            "id"=> 514,
            "number"=> 9,
            "title"=> "Stars and Stripes",
            "season_id"=> 43
        ], [
            "id"=> 515,
            "number"=> 10,
            "title"=> "Crossing the Line",
            "season_id"=> 43
        ], [
            "id"=> 516,
            "number"=> 1,
            "title"=> "Lights Out",
            "season_id"=> 44
        ], [
            "id"=> 517,
            "number"=> 2,
            "title"=> "Boiling Point",
            "season_id"=> 44
        ], [
            "id"=> 518,
            "number"=> 3,
            "title"=> "Dogfight",
            "season_id"=> 44
        ], [
            "id"=> 519,
            "number"=> 4,
            "title"=> "Dark Days",
            "season_id"=> 44
        ], [
            "id"=> 520,
            "number"=> 5,
            "title"=> "Great Expectations",
            "season_id"=> 44
        ], [
            "id"=> 521,
            "number"=> 6,
            "title"=> "Raging Bulls",
            "season_id"=> 44
        ], [
            "id"=> 522,
            "number"=> 7,
            "title"=> "Seeing Red - Few — if any",
            "season_id"=> 44
        ], [
            "id"=> 523,
            "number"=> 8,
            "title"=> "Musical Chairs",
            "season_id"=> 44
        ], [
            "id"=> 524,
            "number"=> 9,
            "title"=> "Blood, Sweat & Tears",
            "season_id"=> 44
        ], [
            "id"=> 525,
            "number"=> 10,
            "title"=> "Checkered Flag",
            "season_id"=> 44
        ], [
            "id"=> 526,
            "number"=> 1,
            "title"=> "Past, Present or Future",
            "season_id"=> 46
        ], [
            "id"=> 527,
            "number"=> 2,
            "title"=> "The Falls Guys",
            "season_id"=> 46
        ], [
            "id"=> 528,
            "number"=> 3,
            "title"=> "Bah humbug-atti",
            "season_id"=> 46
        ], [
            "id"=> 529,
            "number"=> 4,
            "title"=> "Unscripted",
            "season_id"=> 46
        ], [
            "id"=> 530,
            "number"=> 5,
            "title"=> "Up, Down and Round the Farm",
            "season_id"=> 46
        ], [
            "id"=> 531,
            "number"=> 6,
            "title"=> "Jaaaaaaaags",
            "season_id"=> 46
        ], [
            "id"=> 532,
            "number"=> 7,
            "title"=> "It's a Gas, Gas, Gas",
            "season_id"=> 46
        ], [
            "id"=> 533,
            "number"=> 8,
            "title"=> "Blasts from the Past",
            "season_id"=> 46
        ], [
            "id"=> 534,
            "number"=> 9,
            "title"=> "Breaking, badly",
            "season_id"=> 46
        ], [
            "id"=> 535,
            "number"=> 10,
            "title"=> "Oh, Canada",
            "season_id"=> 46
        ], [
            "id"=> 536,
            "number"=> 11,
            "title"=> "Feed the World",
            "season_id"=> 46
        ], [
            "id"=> 537,
            "number"=> 1,
            "title"=> "Motown Funk",
            "season_id"=> 47
        ], [
            "id"=> 538,
            "number"=> 2,
            "title"=> "Colombia Special Part 1",
            "season_id"=> 47
        ], [
            "id"=> 539,
            "number"=> 3,
            "title"=> "Colombia Special Part 2",
            "season_id"=> 47
        ], [
            "id"=> 540,
            "number"=> 4,
            "title"=> "Pick Up, Put Downs",
            "season_id"=> 47
        ], [
            "id"=> 541,
            "number"=> 5,
            "title"=> "An Itchy Urus",
            "season_id"=> 47
        ], [
            "id"=> 542,
            "number"=> 6,
            "title"=> "Chinese Food for Thought",
            "season_id"=> 47
        ], [
            "id"=> 543,
            "number"=> 7,
            "title"=> "Well Aged Scotch",
            "season_id"=> 47
        ], [
            "id"=> 544,
            "number"=> 8,
            "title"=> "International Buffoons' Vacation",
            "season_id"=> 47
        ], [
            "id"=> 545,
            "number"=> 9,
            "title"=> "Aston, Astronauts and Angelina’s children",
            "season_id"=> 47
        ], [
            "id"=> 546,
            "number"=> 10,
            "title"=> "The Youth Vote",
            "season_id"=> 47
        ], [
            "id"=> 547,
            "number"=> 11,
            "title"=> "Sea to Unsalty Sea",
            "season_id"=> 47
        ], [
            "id"=> 548,
            "number"=> 12,
            "title"=> "Legends and Luggage",
            "season_id"=> 47
        ], [
            "id"=> 549,
            "number"=> 13,
            "title"=> "Survival of the Fattest",
            "season_id"=> 47
        ], [
            "id"=> 550,
            "number"=> 14,
            "title"=> "Funeral for a Ford",
            "season_id"=> 47
        ], [
            "id"=> 551,
            "number"=> 1,
            "title"=> "The Holy Trinity",
            "season_id"=> 45
        ], [
            "id"=> 552,
            "number"=> 2,
            "title"=> "Operation Desert Stumble",
            "season_id"=> 45
        ], [
            "id"=> 553,
            "number"=> 3,
            "title"=> "Opera, Art and Donuts",
            "season_id"=> 45
        ], [
            "id"=> 554,
            "number"=> 4,
            "title"=> "Enviro-mental",
            "season_id"=> 45
        ], [
            "id"=> 555,
            "number"=> 5,
            "title"=> "Moroccan Roll",
            "season_id"=> 45
        ], [
            "id"=> 556,
            "number"=> 6,
            "title"=> "Happy Finnish Christmas",
            "season_id"=> 45
        ], [
            "id"=> 557,
            "number"=> 7,
            "title"=> "The Beach (Buggy) Boys - Part 1",
            "season_id"=> 45
        ], [
            "id"=> 558,
            "number"=> 8,
            "title"=> "The Beach (Buggy) Boys - Part 2",
            "season_id"=> 45
        ], [
            "id"=> 559,
            "number"=> 9,
            "title"=> "Berks to the Future",
            "season_id"=> 45
        ], [
            "id"=> 560,
            "number"=> 10,
            "title"=> "Dumb Fight at the O.K. Coral",
            "season_id"=> 45
        ], [
            "id"=> 561,
            "number"=> 11,
            "title"=> "Italian Lessons",
            "season_id"=> 45
        ], [
            "id"=> 562,
            "number"=> 12,
            "title"=> "[censored] to [censored]",
            "season_id"=> 45
        ], [
            "id"=> 563,
            "number"=> 13,
            "title"=> "Past v Future",
            "season_id"=> 45
        ], [
            "id"=> 564,
            "number"=> 1,
            "title"=> "Seamen",
            "season_id"=> 48
        ], [
            "id"=> 565,
            "number"=> 1,
            "title"=> "1=>23=>45",
            "season_id"=> 49
        ], [
            "id"=> 566,
            "number"=> 2,
            "title"=> "Please Remain Calm",
            "season_id"=> 49
        ], [
            "id"=> 567,
            "number"=> 3,
            "title"=> "Open Wide, O Earth",
            "season_id"=> 49
        ], [
            "id"=> 568,
            "number"=> 4,
            "title"=> "The Happiness of All Mankind",
            "season_id"=> 49
        ], [
            "id"=> 569,
            "number"=> 5,
            "title"=> "Vichnaya Pamyat",
            "season_id"=> 49
        ], [
            "id"=> 570,
            "number"=> 1,
            "title"=> "The Six Thatchers",
            "season_id"=> 53
        ], [
            "id"=> 571,
            "number"=> 2,
            "title"=> "The Lying Detective",
            "season_id"=> 53
        ], [
            "id"=> 572,
            "number"=> 3,
            "title"=> "The Final Problem",
            "season_id"=> 53
        ], [
            "id"=> 573,
            "number"=> 1,
            "title"=> "The Empty Hearse",
            "season_id"=> 52
        ], [
            "id"=> 574,
            "number"=> 2,
            "title"=> "The Sign of Three",
            "season_id"=> 52
        ], [
            "id"=> 575,
            "number"=> 3,
            "title"=> "His Last Vow",
            "season_id"=> 52
        ], [
            "id"=> 576,
            "number"=> 1,
            "title"=> "A Scandal in Belgravia",
            "season_id"=> 51
        ], [
            "id"=> 577,
            "number"=> 2,
            "title"=> "The Hounds of Baskerville",
            "season_id"=> 51
        ], [
            "id"=> 578,
            "number"=> 3,
            "title"=> "The Reichenbach Fall",
            "season_id"=> 51
        ], [
            "id"=> 579,
            "number"=> 1,
            "title"=> "A Study in Pink",
            "season_id"=> 50
        ], [
            "id"=> 580,
            "number"=> 2,
            "title"=> "The Blind Banker",
            "season_id"=> 50
        ], [
            "id"=> 581,
            "number"=> 3,
            "title"=> "The Great Game",
            "season_id"=> 50
        ], [
            "id"=> 582,
            "number"=> 1,
            "title"=> "The Crocodile's Dilemma",
            "season_id"=> 54
        ], [
            "id"=> 583,
            "number"=> 2,
            "title"=> "The Rooster Prince",
            "season_id"=> 54
        ], [
            "id"=> 584,
            "number"=> 3,
            "title"=> "A Muddy Road",
            "season_id"=> 54
        ], [
            "id"=> 585,
            "number"=> 4,
            "title"=> "Eating the Blame",
            "season_id"=> 54
        ], [
            "id"=> 586,
            "number"=> 5,
            "title"=> "The Six Ungraspables",
            "season_id"=> 54
        ], [
            "id"=> 587,
            "number"=> 6,
            "title"=> "Buridan's Ass",
            "season_id"=> 54
        ], [
            "id"=> 588,
            "number"=> 7,
            "title"=> "Who Shaves the Barber?",
            "season_id"=> 54
        ], [
            "id"=> 589,
            "number"=> 8,
            "title"=> "The Heap",
            "season_id"=> 54
        ], [
            "id"=> 590,
            "number"=> 9,
            "title"=> "A Fox, a Rabbit, and a Cabbage",
            "season_id"=> 54
        ], [
            "id"=> 591,
            "number"=> 10,
            "title"=> "Morton's Fork",
            "season_id"=> 54
        ], [
            "id"=> 592,
            "number"=> 1,
            "title"=> "The Law of Vacant Places",
            "season_id"=> 56
        ], [
            "id"=> 593,
            "number"=> 2,
            "title"=> "The Principle of Restricted Choice",
            "season_id"=> 56
        ], [
            "id"=> 594,
            "number"=> 3,
            "title"=> "The Law of Non-Contradiction",
            "season_id"=> 56
        ], [
            "id"=> 595,
            "number"=> 4,
            "title"=> "The Narrow Escape Problem",
            "season_id"=> 56
        ], [
            "id"=> 596,
            "number"=> 5,
            "title"=> "The House of Special Purpose",
            "season_id"=> 56
        ], [
            "id"=> 597,
            "number"=> 6,
            "title"=> "The Lord of No Mercy",
            "season_id"=> 56
        ], [
            "id"=> 598,
            "number"=> 7,
            "title"=> "The Law of Inevitability",
            "season_id"=> 56
        ], [
            "id"=> 599,
            "number"=> 8,
            "title"=> "Who Rules the Land of Denial?",
            "season_id"=> 56
        ], [
            "id"=> 600,
            "number"=> 9,
            "title"=> "Aporia",
            "season_id"=> 56
        ], [
            "id"=> 601,
            "number"=> 10,
            "title"=> "Somebody to Love",
            "season_id"=> 56
        ], [
            "id"=> 602,
            "number"=> 1,
            "title"=> "Waiting for Dutch",
            "season_id"=> 55
        ], [
            "id"=> 603,
            "number"=> 2,
            "title"=> "Before the Law",
            "season_id"=> 55
        ], [
            "id"=> 604,
            "number"=> 3,
            "title"=> "The Myth of Sisyphus",
            "season_id"=> 55
        ], [
            "id"=> 605,
            "number"=> 4,
            "title"=> "Fear and Trembling",
            "season_id"=> 55
        ], [
            "id"=> 606,
            "number"=> 5,
            "title"=> "The Gift of the Magi",
            "season_id"=> 55
        ], [
            "id"=> 607,
            "number"=> 6,
            "title"=> "Rhinoceros",
            "season_id"=> 55
        ], [
            "id"=> 608,
            "number"=> 7,
            "title"=> "Did You Do This? No, You Did It!",
            "season_id"=> 55
        ], [
            "id"=> 609,
            "number"=> 8,
            "title"=> "Loplop",
            "season_id"=> 55
        ], [
            "id"=> 610,
            "number"=> 9,
            "title"=> "The Castle",
            "season_id"=> 55
        ], [
            "id"=> 611,
            "number"=> 10,
            "title"=> "Palindrome",
            "season_id"=> 55
        ], [
            "id"=> 612,
            "number"=> 1,
            "title"=> "Episode 1",
            "season_id"=> 57
        ], [
            "id"=> 613,
            "number"=> 2,
            "title"=> "Episode 2",
            "season_id"=> 57
        ], [
            "id"=> 614,
            "number"=> 1,
            "title"=> "Chapter One=> The Vanishing of Will Byers",
            "season_id"=> 58
        ], [
            "id"=> 615,
            "number"=> 2,
            "title"=> "Chapter Two=> The Weirdo on Maple Street",
            "season_id"=> 58
        ], [
            "id"=> 616,
            "number"=> 3,
            "title"=> "Chapter Three=> Holly, Jolly",
            "season_id"=> 58
        ], [
            "id"=> 617,
            "number"=> 4,
            "title"=> "Chapter Four=> The Body",
            "season_id"=> 58
        ], [
            "id"=> 618,
            "number"=> 5,
            "title"=> "Chapter Five=> The Flea and the Acrobat",
            "season_id"=> 58
        ], [
            "id"=> 619,
            "number"=> 6,
            "title"=> "Chapter Six=> The Monster",
            "season_id"=> 58
        ], [
            "id"=> 620,
            "number"=> 7,
            "title"=> "Chapter Seven=> The Bathtub",
            "season_id"=> 58
        ], [
            "id"=> 621,
            "number"=> 8,
            "title"=> "Chapter Eight=> The Upside Down",
            "season_id"=> 58
        ], [
            "id"=> 622,
            "number"=> 1,
            "title"=> "Chapter One=> MADMAX",
            "season_id"=> 59
        ], [
            "id"=> 623,
            "number"=> 2,
            "title"=> "Chapter Two=> Trick or Treat, Freak",
            "season_id"=> 59
        ], [
            "id"=> 624,
            "number"=> 3,
            "title"=> "Chapter Three=> The Pollywog",
            "season_id"=> 59
        ], [
            "id"=> 625,
            "number"=> 4,
            "title"=> "Chapter Four=> Will the Wise",
            "season_id"=> 59
        ], [
            "id"=> 626,
            "number"=> 5,
            "title"=> "Chapter Five=> Dig Dug",
            "season_id"=> 59
        ], [
            "id"=> 627,
            "number"=> 6,
            "title"=> "Chapter Six=> The Spy",
            "season_id"=> 59
        ], [
            "id"=> 628,
            "number"=> 7,
            "title"=> "Chapter Seven=> The Lost Sister",
            "season_id"=> 59
        ], [
            "id"=> 629,
            "number"=> 8,
            "title"=> "Chapter Eight=> The Mind Flayer",
            "season_id"=> 59
        ], [
            "id"=> 630,
            "number"=> 9,
            "title"=> "Chapter Nine=> The Gate",
            "season_id"=> 59
        ], [
            "id"=> 631,
            "number"=> 1,
            "title"=> "Chapter One=> Suzie, Do You Copy?",
            "season_id"=> 60
        ], [
            "id"=> 632,
            "number"=> 2,
            "title"=> "Chapter Two=> The Mall Rats",
            "season_id"=> 60
        ], [
            "id"=> 633,
            "number"=> 3,
            "title"=> "Chapter Three=> The Case of the Missing Lifeguard",
            "season_id"=> 60
        ], [
            "id"=> 634,
            "number"=> 4,
            "title"=> "Chapter Four=> The Sauna Test",
            "season_id"=> 60
        ], [
            "id"=> 635,
            "number"=> 5,
            "title"=> "Chapter Five=> The Flayed",
            "season_id"=> 60
        ], [
            "id"=> 636,
            "number"=> 6,
            "title"=> "Chapter Six=> E Pluribus Unum",
            "season_id"=> 60
        ], [
            "id"=> 637,
            "number"=> 7,
            "title"=> "Chapter Seven=> The Bite",
            "season_id"=> 60
        ]];

        foreach($episodes as $episode) {
            App\Episode::create($episode);
        }
    }
}
