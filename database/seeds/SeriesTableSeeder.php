<?php

use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $series = [ 
            [ 
            'id'=>1,
            'title'=>'Eigen Kweek',
            'plot_en'=>'A farmer loses all his money and decides to grow weed to compensate for the losses.',
            'plot_nl'=>'Een boer verliest al zijn spaargeld en besluit zijn schulden weg te werken door wiet te kweken',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMjM2MzY4MTk4M15BMl5BanBnXkFtZTgwMTQwOTI0MzE@._V1_SX300.jpg'
            ],
            [ 
            'id'=>2,
            'title'=>'Money Heist',
            'plot_en'=>'An unusual group of robbers attempt to carry out the most perfect robbery in Spanish history - stealing 2.4 billion euros from the Royal Mint of Spain.',
            'plot_nl'=> 'Een ongewone groep van overvallers probeert de meest perfecte overval uit te voeren in de Spaanse geschiedenis: 2,4 miljard euro stelen uit de Koninklijke Munt van Spanje',   
            'poster'=>'https://m.media-amazon.com/images/M/MV5BZDcxOGI0MDYtNTc5NS00NDUzLWFkOTItNDIxZjI0OTllNTljXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg'
            ],
            [ 
            'id'=>3,
            'title'=>'Breaking Bad',
            'plot_en'=>'A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family\'s future.',
            'plot_nl'=>'Een scheikunde leraar met ongeneesbare longkanker begint met het maken en verkopen van methamfetamine om zijn families toekomst te verzekeren',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMjhiMzgxZTctNDc1Ni00OTIxLTlhMTYtZTA3ZWFkODRkNmE2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg'
            ],
            [ 
            'id'=>4,
            'title'=>'Peaky Blinders',
            'plot_en'=>'A gangster family epic set in 1919 Birmingham, England; centered on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby.',
            'plot_nl'=>'Een familie van gangsters in Birmingham (Engeland) in 1919, die scheermesjes in hun petter naaiden en hun felle baas Tommy Shely',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMTkzNjEzMDEzMF5BMl5BanBnXkFtZTgwMDI0MjE4MjE@._V1_SX300.jpg'
            ],
            [ 
            'id'=>5,
            'title'=>'Game of Thrones',
            'plot_en'=>'Nine noble families fight for control over the mythical lands of Westeros, while an ancient enemy returns after being dormant for thousands of years.',
            'plot_nl'=>'Negen nobele families vechten om de controle over het mythische land Westeros, terwijl een oude vijand terugkeert na duizenden jaren geslapen te hebben',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMjA5NzA5NjMwNl5BMl5BanBnXkFtZTgwNjg2OTk2NzM@._V1_SX300.jpg'
            ],
            [ 
            'id'=>6,
            'title'=>'Vikings',
            'plot_en'=>'Vikings transports us to the brutal and mysterious world of Ragnar Lothbrok, a Viking warrior and farmer who yearns to explore - and raid - the distant shores across the ocean.',
            'plot_nl'=>'Vikings brengt ons tot de harde en mysterieuze wereld van Ragnar Lothbrok, een Viking strijder en boer, die verlangt naar het ontdekken en aanvallen van verre stranden aan de andere kant van de oceaan',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BNjIzZjljZmQtOGNiYi00YmY2LWE1MGYtN2VlMmEyZDBlMzRmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg'
            ],
            [ 
            'id'=>7,
            'title'=>'Tiger King',
            'plot_en'=>'A rivalry between big cat eccentrics takes a dark turn when Joe Exotic, a controversial animal park boss, is caught in a murder-for-hire plot.',
            'plot_nl'=>'Een rivaliteit tussen liefhebbers van grote katten neemt een donkere draai wanneer Joe Exotic, een controversiele dierenparkeigenaar, gepakt wordt in een huurmoord plot',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BYzI5MjQ2NzEtN2JmOC00MjE2LWI2NjItYTNjNTJjMjBkOWZkXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg'
            ],
            [ 
            'id'=>8,
            'title'=>'The Confession Killer',
            'plot_en'=>'Henry Lee Lucas was known as America\'s most prolific serial killer, admitting to hundreds of murders, but, as DNA results contradict his confessions, will they expose the biggest criminal justice hoax in U.S. history?',
            'plot_nl'=>'Henry Lee Lucas stond bekand als de grootste seriemoordenaar van Amerika. Hij bekende honderden moorden, maar als DNA resultaten zijn bekentenissen in twijfel brengen, ontstaat er een kans om de grootste hoax in de Amerikaanse geschiedenis blood te leggen',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMzM3NzQyYmItOTNjZS00MTVmLThhMWEtOGYxNDkzNTBiOTkyXkEyXkFqcGdeQXVyMzEzMDM1ODA@._V1_SX300.jpg'
            ],
            [ 
            'id'=>9,
            'title'=>'Don\'t F**k with Cats: Hunting an Internet Killer',
            'plot_en'=>'A group of online justice seekers track down a guy who posted a video of himself killing kittens.',
            'plot_nl'=>'Een groep gerechtigheidszoekers online, achterhalen informatie over een gast die een video van zichzelf plaatste waarin hij kittens vermoord',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BNGU2OGJkZTItYmRmNi00YTI2LWFkNzEtNjY2MGZiZTRhMzRkXkEyXkFqcGdeQXVyMjYwNDA2MDE@._V1_SX300.jpg'
            ],
            [ 
            'id'=>10,
            'title'=>'Prison Break',
            'plot_en'=>'Due to a political conspiracy, an innocent man is sent to death row and his only hope is his brother, who makes it his mission to deliberately get himself sent to the same prison in order to break the both of them out, from the inside.',
            'plot_nl'=>'Door een politiek complot wordt een onschuldige man tot de dood veroordeeld en zijn enige hoop is zijn broer, die het zijn missie maakt om zichzelf op te laten sluiten en van binnenuit zijn broer en zichzelf te bevrijden.',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMTg3NTkwNzAxOF5BMl5BanBnXkFtZTcwMjM1NjI5MQ@@._V1_SX300.jpg'
            ],
            [ 
            'id'=>11,
            'title'=>'Narcos',
            'plot_en'=>'A chronicled look at the criminal exploits of Colombian drug lord Pablo Escobar, as well as the many other drug kingpins who plagued the country through the years.',
            'plot_nl'=>'Neem een kijk in de criminele praktijken van de Colombiaanse drugsbaron Pablo Escobar, tezamen met de vele andere drugsbazen die Amerika bezig hielden doorheen de jaren',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BNmFjODU3YzgtMGUwNC00ZGI3LWFkZjQtMjkxZDc3NmQ1MzcyXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SX300.jpg'
            ],
            [ 
            'id'=>12,
            'title'=>'Sex Education',
            'plot_en'=>'A teenage boy with a sex therapist mother teams up with a high school classmate to set up an underground sex therapy clinic at school.',
            'plot_nl'=>'Een tienerjongen, wiens moeder sekstherapeut is, vormt een team met klasgenoot en start een ondergrondse sekstherapie onderneming op school.',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BZjgyMzFiMDgtNWNmMS00ZDEyLTkzYzgtMjMzZjk4YjhjZWUxXkEyXkFqcGdeQXVyNDg4MjkzNDk@._V1_SX300.jpg'
            ],
            [ 
            'id'=>13,
            'title'=>'Formula 1: Drive to Survive',
            'plot_en'=>'Docuseries following the 2018 FIA Formula One World Championship.',
            'plot_nl'=>'Documantaire waarin de 2018 FIA Formule 1 wereldkampioenschappen gevolgd worden',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMzVkMGU0YWMtOWQxMC00MjFhLTg1NjAtMDFlZTZlYzJlMjlhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg'
            ],
            [ 
            'id'=>14,
            'title'=>'The Grand Tour',
            'plot_en'=>'Follow Jeremy, Richard, and James, as they embark on an adventure across the globe, driving new and exciting automobiles from manufacturers all over the world.',
            'plot_nl'=>'Volg Jeremy, Richard, en James op hun avonturen rond de wereld, waarin ze met nieuwe en spannende auto\'s rijden.',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BYjkyOWIyZGYtYzU3ZS00NWM2LThjZGEtMDZjZjg2MTI2NzBhXkEyXkFqcGdeQXVyNjI4OTg2Njg@._V1_SX300.jpg'
            ],
            [ 
            'id'=>15,
            'title'=>'Chernobyl',
            'plot_en'=>'In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world\'s worst man-made catastrophes.',
            'plot_nl'=>'In april 1986 vindt er een explosie plaats in de kerncentrale van Tsjernobyl in de toenmalige Sovjet Unie, en draait uit op één van werelds grootste door de mens gemaakte rampen.',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BZGQ2YmMxZmEtYjI5OS00NzlkLTlkNTEtYWMyMzkyMzc2MDU5XkEyXkFqcGdeQXVyMzQ2MDI5NjU@._V1_SX300.jpg'
            ],
            [ 
            'id'=>16,
            'title'=>'Sherlock',
            'plot_en'=>'A modern update finds the famous sleuth and his doctor partner solving crime in 21st century London.',
            'plot_nl'=>'Een moderne update vind de beroemde speurneus en zijn dokter die misdaden oplossen in Londen in de 21ste eeuw.',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BMWY3NTljMjEtYzRiMi00NWM2LTkzNjItZTVmZjE0MTdjMjJhL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNTQ4NTc5OTU@._V1_SX300.jpg'
            ],
            [ 
            'id'=>17,
            'title'=>'Fargo',
            'plot_en'=>'Various chronicles of deception, intrigue and murder in and around frozen Minnesota. Yet all of these tales mysteriously lead back one way or another to Fargo, North Dakota.',
            'plot_nl'=>'Meerdere kronieken van bedrog, verraad en moord in het bevroren Minnesota. Toch leiden al deze verhalen op mysterieze wijze terug naar Fargo in North Dakote',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BNTUyYjZiOTQtY2VhZC00MzdhLWJlNzctZjQyYjk1ZTZmOTZiXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg'
            ],
            [ 
            'id'=>18,
            'title'=>'Stranger Things',
            'plot_en'=>'When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.',
            'plot_nl'=>'Wanneer een jonge jongen verdwijnt, moeten zijn moeder, een politieagent, en zijn vrienden angstaanjagende bovennatuurlijke krachten confronteren om hem terug te krijgen.',
            'poster'=>'https://m.media-amazon.com/images/M/MV5BZGExYjQzNTQtNGNhMi00YmY1LTlhY2MtMTRjODg3MjU4YTAyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg'
            ]
        ];
    
        foreach($series as $elem) {
            App\Series::create($elem);
        }
    
        }
}
