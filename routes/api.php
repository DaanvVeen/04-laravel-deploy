<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login')->name("auth.login");
Route::post('register', 'AuthController@register')->name("auth.register");
Route::post('logout', 'AuthController@logout')->name("auth.logout");
Route::post('refresh', 'AuthController@refresh')->name("auth.refresh");
Route::get('me', 'AuthController@me')->name("auth.me");

Route::middleware(['auth.jwt'])->group( function () {
    // Get all series in the database
    Route::get('series', 'SeriesController@all');
    // Search for a series with a keyword
    Route::get('series/search/{keyword}', 'SeriesController@search');
    // Get the details from a certain series by its id
    Route::get('series/{id}/details', 'SeriesController@details');
    // Get a list of seasons for a certain series by its id
    Route::get('series/{id}/seasons', 'SeriesController@seasons');
    // Get a list of episodes for a certain season by its id
    Route::get('season/{id}/episodes', 'SeasonsController@episodes');

    // Start following a certain series
    Route::post('series/{id}/follow', 'UserSeriesController@follow');
    // Unfollow a certain series
    Route::post('series/{id}/unfollow', 'UserSeriesController@unfollow');
    // Flag a series as finished
    Route::post('series/{id}/finished', 'UserSeriesController@finished');
    // Get all series the user follows
    Route::get('series/following', 'UserSeriesController@getFollowing');

    // Flag an episode as seen
    Route::post('episode/{id}/seen', 'EpisodeController@seen');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
